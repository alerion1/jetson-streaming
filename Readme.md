# Video Streaming on nVidia Jetson Nano

## Context

There are three main ways to stream video from the Jetson Nano:

- Using gstreamer. It is bunlded with the default Tegra image and is easy to use in C with the provided libraries.
- Using a fork of ffmpeg. The version of ffmpeg bundled with the Jetson Nano is not able to use the hardware encoder thus making it unsuitable. Instead a fork can be used that contains the proper plugin: https://github.com/jocover/jetson-ffmpeg
- Using the native [Jetson Multimedia API](https://docs.nvidia.com/jetson/l4t-multimedia/) and Argus. This is the hardest method to implement as a lot of low-level code has to be written. This method is the most powerful however and can extract every bit of performance from the hardware.


## Jetson Multimedia API

To use the Jetson Multimedia API to stream video, a pipeline must be created for the data.

First in the pipeline is the image aquisition. This is done using libargus. A capture session is created and customized for the desired camera behavior. Buffer are created to allow the EGL buffers rendered by Argus to be used by our encoder. The number of download buffers must be chosen carefully to prevent stutter (not enough buffers) and latency (too many buffers).

The Argus buffers obtained are transformed on reception to scale, rotate, zoom, mirror and crop the raw camera image.

When the transforms are done, the buffer is transfered *via* [DMA](https://en.wikipedia.org/wiki/Direct_memory_access) to the *v4l2* kernel API that handles the interface with the hardware encoders.
Buffers are once again necessary to allow the pipeline to function without starvation, however one to two buffers should be acceptable for this step as the hardware encoders processing speed is quite fast compared to image acquition

When the image is encoded, the resulting buffer that is passed to the encoding callback can be sent as-is on the network or encrypted for security. It is useful to keep the exact frame timestamp with it to reduce stutter when displaying it.

## Contributors

Antoine Richard - antoine.richard@alerion.fr
